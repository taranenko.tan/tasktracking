﻿using Data.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IGenericRepository<Tkey, TEntity> 
        where TEntity : BaseClass
    {
        Task< IQueryable<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(Tkey id);
        Task<TEntity> GetEntityAsync(Expression<Func<TEntity, bool>> expression);
        Task CreateAsync(TEntity entity);
        void Update(TEntity entity);
        Task DeleteByIdAsync(Tkey id);
        //just for training purpose to see how status can be changed
        TaskTrackingContext Context { get; }

    }
}
