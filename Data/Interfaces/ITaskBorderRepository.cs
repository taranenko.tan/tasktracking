﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface ITaskBorderRepository : IGenericRepository<int, TaskBoard>
    {
        Task<IQueryable<TaskBoard>> GetAllWithDetails();
        Task<TaskBoard> GetByIdWithDetailsAsync(int id);

    }
}
