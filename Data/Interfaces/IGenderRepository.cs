﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IGenderRepository : IGenericRepository<int, Gender>
    {
        void AttachEmployees(IEnumerable<Employee> employees);
        void AttachGender(Gender gender);
        Task<IQueryable<Gender>> GetAllWithDetailsAsync();
        Task<Gender> GetByIdWithDetailsAsync(int id);
    }
}

