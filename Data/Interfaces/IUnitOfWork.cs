﻿using Data.Entities;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        IGenderRepository GenderRepository { get; }
        IEmploeyyRepository EmployeeRepository { get; }
        IMissionRepository MissionRepository { get; }
        ITaskBorderRepository TaskBorderRepository { get; }
        IRoleRepository RoleRepository { get; }
        IEmpolyeeInRole EmpolyeeInRoleRepository { get; }
        IStatusRepository StatusRepository { get; }
        ICommentRepository CommentRepository { get; }
        Task<int> SaveAsync();
        //just for training purpose to see how status can be changed
        TaskTrackingContext Context { get; }

    }
}
