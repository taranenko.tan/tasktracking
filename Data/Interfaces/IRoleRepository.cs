﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IRoleRepository : IGenericRepository<int, Role>
    {
        Task<IQueryable<Role>> GetAllWithDetailsAsync();
    }
}
