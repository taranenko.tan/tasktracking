﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IEmpolyeeInRole : IGenericRepository<int, EmployeeInRole>
    {
        Task<IQueryable<EmployeeInRole>> GetAllWithDetailsAsync();
        Task<IQueryable<EmployeeInRole>> GetByEmployeeIdAsync(int id);
    }
}
