﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IMissionRepository : IGenericRepository<int, Mission>
    {
        Task<IQueryable<Mission>> GetAllWithDetailsAsync();
        Task<Mission> GetByIdWithDetailsAsync(int id);
    }
}
