﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class TaskBoard : BaseClass
    {
        public int IssuedByInRoleId { get; set; }
        public  int EmployeeInRoleId { get; set; }
        public  int MissionId { get; set; }

        public virtual EmployeeInRole EmployeeInRole { get; set; }
        public virtual EmployeeInRole IssuedByInRole { get; set; }
        public virtual Mission Mission { get; set; }


    }
}
