﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Comment : BaseClass
    {
        public string Opus { get; set; }

        public int MissionId { get; set; }

        public virtual Mission Mission { get; set; }
    }
}
