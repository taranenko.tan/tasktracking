﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Gender : BaseClass
    {
        public string Title { get; set; }
        public virtual IEnumerable<Employee> Employees { get; set; }
    }
}
