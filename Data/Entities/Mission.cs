﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Mission : BaseClass
    {
        public virtual int? StatusId { get; set; }
        public DateTime BeginMission { get; set; }
        public DateTime FinishMission { get; set; }
        public string Discription { get; set; }
        public string Completion { get; set; }


        public virtual Status Status { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
        public virtual IEnumerable<TaskBoard> TaskBoards { get; set; }

    }
}
