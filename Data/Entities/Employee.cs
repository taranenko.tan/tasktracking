﻿using System;
using System.Collections.Generic;

namespace Data.Entities
{
    public class Employee : BaseClass
    {
        public int GenderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string HashPassword { get; set; }
        public string Email { get; set; }


        public virtual Gender Gender { get; set; }
        public virtual IEnumerable<EmployeeInRole> EmployeeInRole { get; set; }



    }
}
