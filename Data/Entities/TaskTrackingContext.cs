﻿using Microsoft.EntityFrameworkCore;

namespace Data.Entities
{
    public class TaskTrackingContext : DbContext
    {
        public TaskTrackingContext(DbContextOptions<TaskTrackingContext> opt) : base(opt)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.LazyLoadingEnabled = false;
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=TaskTracking;MultipleActiveResultSets=true");
        //}


        public DbSet<Comment> Comments { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<TaskBoard> TaskBoards { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeInRole> EmployeeInRoles { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Role>()
                .HasMany(r => r.UserInRoles)
                .WithOne(u => u.Role)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<TaskBoard>()
                .HasOne(t => t.IssuedByInRole)
                .WithMany(u => u.Managers)
                .HasForeignKey(t => t.IssuedByInRoleId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TaskBoard>()
                .HasOne(t => t.EmployeeInRole)
                .WithMany(u => u.Employee)
                .HasForeignKey(t => t.EmployeeInRoleId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
