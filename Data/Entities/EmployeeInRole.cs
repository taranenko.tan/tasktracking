﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class EmployeeInRole : BaseClass
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public virtual Employee User { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<TaskBoard> Managers { get; set; }
        public virtual ICollection<TaskBoard> Employee { get; set; }
    }
}
