﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Role : BaseClass
    {
        public string RoleName { get; set; }
        public virtual IEnumerable<EmployeeInRole> UserInRoles { get; set; }


    }
}
