﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Status : BaseClass 
    {
        public string StatusName { get; set; }

        public virtual IEnumerable<Mission> Missions { get; set; }
    }
}
