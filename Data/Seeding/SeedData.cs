﻿using Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Seeding
{
    public class SeedData
    {
        public static void SeedDatabase(TaskTrackingContext context, IPasswordHasher<Employee> hasher)
        {
            context.Database.Migrate();

            if (context.Genders.Count() == 0)
            {
                var male = new Gender { Title = "Male" };
                var female = new Gender { Title = "Female" };

                var admin = new Role { RoleName = "Admin" };
                var manager = new Role { RoleName = "Manager" };
                var worker = new Role { RoleName = "Worker" };

                var employee1 = new Employee { FirstName = "Liam", LastName = "Smith", Gender = male, Email = "liam@gmail.com", Birthday = new DateTime(1987, 10, 10) };
                employee1.HashPassword = hasher.HashPassword(employee1, "123$Abc");
                var employee2 = new Employee { FirstName = "Olivia", LastName = "Johnson", Gender = female, Email = "olivia@gmail.com", Birthday = new DateTime(1995, 12, 12) };
                employee2.HashPassword = hasher.HashPassword(employee1, "123$Abc");
                var employee3 = new Employee { FirstName = "Noah", LastName = "Williams", Gender = male, Email = "noah@gmail.com", Birthday = new DateTime(1994, 4, 25) };

                var role1 = new EmployeeInRole { User = employee1, Role = admin };
                var role2 = new EmployeeInRole { User = employee2, Role = manager };
                var role3 = new EmployeeInRole { User = employee3, Role = worker };

                context.EmployeeInRoles.AddRange(role1, role2, role3);

                //var closed = new Status { StatusName = "Closed" };
                var inProcess = new Status { StatusName = "InProcess" };
                //var notStarted = new Status { StatusName = "NotStarted" };
                //var canceled = new Status { StatusName = "Canceled" };

                var comment1 = new Comment { Opus = "Computer and information systems managers generally oversee the information technology departments within businesses and organizations. A systems manager’s duties depend on organization size and how much technology they use on a daily basis. " };
                //var comment2 = new Comment { Opus = "In smaller settings, systems managers may offer support on an as-needed basis, while larger organizations may require larger IT departments with more hands-on systems manager roles." };

                var task1 = new Mission
                {
                    Status = inProcess,
                    BeginMission = new DateTime(2021, 10, 10),
                    Completion = "10%",
                    Discription = "Task-1",
                    Comments = new List<Comment> { comment1 }
                };

                var taskBoard1 = new TaskBoard { IssuedByInRole = role1, EmployeeInRole = role3, Mission = task1 };
                context.TaskBoards.AddRange(taskBoard1);

            }
            context.SaveChanges();
        }
    }


}

