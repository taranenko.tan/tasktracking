﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class EmployeeInRoleRepository : GenericRepository<int, EmployeeInRole>, IEmpolyeeInRole
    {
        public EmployeeInRoleRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }

        public async Task<IQueryable<EmployeeInRole>> GetAllWithDetailsAsync()
        {
            var all = await base.GetAllAsync();

            return all.Include(e => e.Employee).Include(e => e.Managers)
                .Include(e => e.Role).Include(e=>e.User);
        }

        public async Task<IQueryable<EmployeeInRole>> GetByEmployeeIdAsync(int id)
        {
            var all = await base.GetAllAsync();
            return all.Where(u => u.UserId == id);
        }
    }
}
