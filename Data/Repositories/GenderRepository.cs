﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class GenderRepository : GenericRepository<int, Gender>, IGenderRepository
    {
        private readonly TaskTrackingContext dbContext;

        public GenderRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IQueryable<Gender>> GetAllWithDetailsAsync()
        {
            var res = await base.GetAllAsync();
            
            return res.Include(g => g.Employees);
        }

        public async Task<Gender> GetByIdWithDetailsAsync(int id)
        {
            var all = await GetAllWithDetailsAsync();
            return await all.FirstOrDefaultAsync(g => g.Id == id);
        }

        public void AttachEmployees(IEnumerable<Employee> employees)
        {
            dbContext.AttachRange(employees);
        }

        public void AttachGender(Gender gender)
        {
            dbContext.Genders.Attach(gender);
        }

    }
}
