﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class GenericRepository<TKey, TEntity> : IGenericRepository<TKey, TEntity> where TEntity : BaseClass
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly TaskTrackingContext _context;

        public TaskTrackingContext Context => _context;

        public GenericRepository(TaskTrackingContext dbContext)
        {
            _dbSet = dbContext.Set<TEntity>();
            _context = dbContext;
        }
        public Task<IQueryable<TEntity>> GetAllAsync()
        {
            return Task.FromResult(_dbSet.AsQueryable());
        }
        public async Task<TEntity> GetByIdAsync(TKey id)
        {
            return await _dbSet.FindAsync(id);
        }
        public async Task<TEntity> GetEntityAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await _dbSet.FirstOrDefaultAsync(expression);
        }
        public async Task CreateAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }
        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }
        public async Task DeleteByIdAsync(TKey id)
        {
            var entity = await _dbSet.FindAsync(id);
            _dbSet.Remove(entity);
        }


    }
}
