﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TaskBorderRepository : GenericRepository<int, TaskBoard>, ITaskBorderRepository
    {
        public TaskBorderRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }

        public async Task< IQueryable<TaskBoard>> GetAllWithDetails()
        {
            var all = await base.GetAllAsync();
            return all.Include(t => t.IssuedByInRole).ThenInclude(i=>i.User)
                    .Include(t => t.EmployeeInRole).ThenInclude(e=>e.User)
                    .Include(t => t.Mission);
        }

        public async Task<TaskBoard> GetByIdWithDetailsAsync(int id)
        {
            var all = await GetAllWithDetails();
            return await all.FirstOrDefaultAsync(b => b.Id == id);
        }
    }
}
