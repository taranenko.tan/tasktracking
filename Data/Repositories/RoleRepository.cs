﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class RoleRepository : GenericRepository<int, Role>, IRoleRepository
    {
        public RoleRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }

        public async Task<IQueryable<Role>> GetAllWithDetailsAsync()
        {
            var all = await base.GetAllAsync();
            return all.Include(r => r.UserInRoles).AsQueryable();
        }
    }
}
