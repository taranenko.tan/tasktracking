﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class EmployeesRepository : GenericRepository<int, Employee>, IEmploeyyRepository
    {
        public EmployeesRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }

        public async Task<IQueryable<Employee>> GetAllWithDetailsAsync()
        {
            var all = await base.GetAllAsync();
            var en = all.AsEnumerable();
            return all.Include(u => u.Gender)
                    .Include(u => u.EmployeeInRole).ThenInclude(e=>e.Role);
        }

        public async Task<Employee> GetByIdWithDetailsAsync(int id)
        {
            return await GetByIdAsync(id);
        }
    }
}
