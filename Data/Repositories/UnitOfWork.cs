﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TaskTrackingContext _context;
        public TaskTrackingContext Context => _context;

        public UnitOfWork(TaskTrackingContext context)
        {
            _context = context;
            GenderRepository = new GenderRepository(context);
            EmployeeRepository = new EmployeesRepository(context);
            MissionRepository = new MissionRepository(context);
            TaskBorderRepository = new TaskBorderRepository(context);
            RoleRepository = new RoleRepository(context);
            EmpolyeeInRoleRepository = new EmployeeInRoleRepository(context);
            StatusRepository = new StatusRepository(context);
            CommentRepository = new CommentRepository(context);
        }


        public IGenderRepository GenderRepository { get; }
        public IEmploeyyRepository EmployeeRepository { get; }

        public IMissionRepository MissionRepository { get; }

        public ITaskBorderRepository TaskBorderRepository { get; }

        public IRoleRepository RoleRepository { get; }

        public IEmpolyeeInRole EmpolyeeInRoleRepository { get; }

        public IStatusRepository StatusRepository { get; }

        public ICommentRepository CommentRepository { get; }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
