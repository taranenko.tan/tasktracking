﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class MissionRepository : GenericRepository<int, Mission>, IMissionRepository
    {
        public MissionRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }

        public async Task<IQueryable<Mission>> GetAllWithDetailsAsync()
        {
            var all = await base.GetAllAsync();
            return all.Include(m => m.Comments)
                       .Include(m => m.Status);
        }

        public async Task<Mission> GetByIdWithDetailsAsync(int id)
        {
            var all = await GetAllWithDetailsAsync();
            return await all.FirstOrDefaultAsync(m => m.Id == id);
        }

    }
}
