﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class StatusRepository : GenericRepository<int, Status>, IStatusRepository
    {
        public StatusRepository(TaskTrackingContext dbContext) : base(dbContext)
        {
        }
    }
}
