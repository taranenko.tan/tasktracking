﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class ChangeTaskBoardPropertiesNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_EmployeeId",
                table: "TaskBoards");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_IssuedById",
                table: "TaskBoards");

            migrationBuilder.DropIndex(
                name: "IX_TaskBoards_EmployeeId",
                table: "TaskBoards");

            migrationBuilder.DropIndex(
                name: "IX_TaskBoards_IssuedById",
                table: "TaskBoards");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "TaskBoards");

            migrationBuilder.DropColumn(
                name: "IssuedById",
                table: "TaskBoards");

            migrationBuilder.AddColumn<int>(
                name: "EmployeeInRoleId",
                table: "TaskBoards",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IssuedByInRoleId",
                table: "TaskBoards",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TaskBoards_EmployeeInRoleId",
                table: "TaskBoards",
                column: "EmployeeInRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskBoards_IssuedByInRoleId",
                table: "TaskBoards",
                column: "IssuedByInRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_EmployeeInRoleId",
                table: "TaskBoards",
                column: "EmployeeInRoleId",
                principalTable: "EmployeeInRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_IssuedByInRoleId",
                table: "TaskBoards",
                column: "IssuedByInRoleId",
                principalTable: "EmployeeInRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_EmployeeInRoleId",
                table: "TaskBoards");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_IssuedByInRoleId",
                table: "TaskBoards");

            migrationBuilder.DropIndex(
                name: "IX_TaskBoards_EmployeeInRoleId",
                table: "TaskBoards");

            migrationBuilder.DropIndex(
                name: "IX_TaskBoards_IssuedByInRoleId",
                table: "TaskBoards");

            migrationBuilder.DropColumn(
                name: "EmployeeInRoleId",
                table: "TaskBoards");

            migrationBuilder.DropColumn(
                name: "IssuedByInRoleId",
                table: "TaskBoards");

            migrationBuilder.AddColumn<int>(
                name: "EmployeeId",
                table: "TaskBoards",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IssuedById",
                table: "TaskBoards",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TaskBoards_EmployeeId",
                table: "TaskBoards",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskBoards_IssuedById",
                table: "TaskBoards",
                column: "IssuedById");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_EmployeeId",
                table: "TaskBoards",
                column: "EmployeeId",
                principalTable: "EmployeeInRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskBoards_EmployeeInRoles_IssuedById",
                table: "TaskBoards",
                column: "IssuedById",
                principalTable: "EmployeeInRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
