﻿using AutoMapper;
using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Validation
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            //Gender
            CreateMap<Gender, GenderDto>()
                .ForMember(genderDTO => genderDTO.EmployeesId, ids =>
                    ids.MapFrom(gender => gender.Employees.Select(u => u.Id)));

            CreateMap<GenderDto, Gender>();

            CreateMap<Gender, Gender>().ForMember(dest => dest.Id, id => id.Ignore());

            //Role
            CreateMap<Role, RoleDto>()
                .ForMember(roleDTIO => roleDTIO.RoleName, name =>
                name.MapFrom(role => role.RoleName))
                .ReverseMap();

            //Employee
            CreateMap<Employee, EmployeeDto>()
                .ForMember(userDTO => userDTO.Gender, gender =>
                    gender.MapFrom(user => user.Gender.Title))
                .ForMember(userDTO => userDTO.EmployeeInRole, roleId =>
                    roleId.MapFrom(user => user.EmployeeInRole.Select(u => u.Id)))
                .ForMember(userDTO => userDTO.Age, age =>
                    age.MapFrom(user => DateTime.Now.Subtract(user.Birthday).TotalDays / 365))
                .ForMember(userDTO => userDTO.Birthday, bithday =>
                    bithday.MapFrom(user => user.Birthday.ToShortDateString()))
                .ForMember(userDTO=> userDTO.Roles,roles => 
                    roles.MapFrom(user=>user.EmployeeInRole.Select(e=>e.Role.RoleName)));

            CreateMap<EmployeeDto, Employee>()
                .ForMember(dest => dest.Id, id => id.Ignore())
                .ForMember(dest => dest.Gender, gender => gender.Ignore())
                .ForMember(dest => dest.GenderId, gId => gId.Ignore())
                .ForMember(dest => dest.HashPassword, hash => hash.Ignore())
                .ForMember(dest => dest.Email, email => email.Ignore())
                .ForMember(dest => dest.EmployeeInRole, emInRol => emInRol.Ignore());


            //EmployeeInRole
            CreateMap<EmployeeInRole, EmployeesInRolesDto>()
                .ForMember(dest=>dest.UserName, name=>
                    name.MapFrom(s=>s.User.FirstName))
                 .ForMember(dest => dest.RoleName, name =>
                    name.MapFrom(s => s.Role.RoleName))
                .ReverseMap();

            CreateMap<EmployeeInRole, EmployeeInRole>()
                .ForMember(dest => dest.Id, id => id.Ignore())
                .ForMember(dest => dest.Role, r => r.Ignore())
                .ForMember(dest => dest.User, u => u.Ignore());

            //Comment
            CreateMap<Comment, CommentDto>()
                .ReverseMap()
                .ForMember(dest => dest.Mission, m => m.Ignore());

            CreateMap<Comment, Comment>().ForMember(dest => dest.Id, id => id.Ignore());

            //Status
            CreateMap<Status, StatusDto>().ReverseMap();

            //Task
            CreateMap<Mission, TaskDto>()
                .ForMember(taskDTO => taskDTO.BeginMission, start =>
                    start.MapFrom(task => task.BeginMission.ToShortDateString()))
                .ForMember(taskDTO => taskDTO.FinishMission, finish =>
                    finish.MapFrom(task => task.FinishMission.ToShortDateString()))
                 .ForMember(taskDTO => taskDTO.Status, status =>
                    status.MapFrom(task => task.StatusId))
                 .ForMember(taskDTO => taskDTO.Comments, comments =>
                    comments.MapFrom(task => task.Comments.Select(c => c.Opus)));

            CreateMap<TaskDto, Mission>()
                .ForMember(mission => mission.StatusId, id =>
                    id.MapFrom(source => source.Status))
                .ForMember(mission => mission.Status, s => s.Ignore());


            CreateMap<Mission, Mission>()
                .ForMember(dest => dest.Id, i => i.Ignore())
                .ForMember(dest => dest.Status, s => s.Ignore())
                .ForMember(dest => dest.StatusId, s => s.Ignore());

            //TaskBoard
            CreateMap<TaskBoard, TaskBoardDto>()
                .ForMember(taskBoardDTO => taskBoardDTO.EmployeeInRoleId, id =>
                     id.MapFrom(taskBoard => taskBoard.EmployeeInRoleId))
                .ForMember(taskBoardDTO => taskBoardDTO.IssuedByInRoleId, id =>
                    id.MapFrom(taskBoard => taskBoard.IssuedByInRoleId))
                .ForMember(taskBoardDTO => taskBoardDTO.Task, id =>
                    id.MapFrom(taskBoard => taskBoard.Mission.Id))
                .ForMember(taskBoardDTO => taskBoardDTO.ManagerName, name=>
                    name.MapFrom(taskBoard=> taskBoard.IssuedByInRole.User.FirstName))
                .ForMember(taskBoardDTO => taskBoardDTO.EmployeeName, name =>
                    name.MapFrom(taskBoard => taskBoard.EmployeeInRole.User.FirstName))
                .ForMember(TaskBoardDto => TaskBoardDto.TaskTitle, title=>
                    title.MapFrom(taskBoard=> taskBoard.Mission.Discription));

            CreateMap<TaskBoardDto, TaskBoard>()
                .ForMember(dest => dest.IssuedByInRoleId, i => i.MapFrom(source => source.IssuedByInRoleId))
                .ForMember(dest => dest.EmployeeInRoleId, i => i.MapFrom(source => source.EmployeeInRoleId))
                .ForMember(dest => dest.MissionId, i => i.MapFrom(source => source.Task))
                .ForMember(dest => dest.EmployeeInRole, e => e.Ignore())
                .ForMember(dest => dest.IssuedByInRole, i => i.Ignore());


            CreateMap<TaskBoard, TaskBoard>()
                .ForMember(dest => dest.Id, id => id.Ignore())
                .ForMember(dest => dest.EmployeeInRole, e => e.Ignore())
                .ForMember(dest => dest.IssuedByInRole, i => i.Ignore())
                .ForMember(dest => dest.Mission, m => m.Ignore());
        }
    }
}
