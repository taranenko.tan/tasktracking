﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Business.Exceptions
{
    public class DoNotCorrectIdException : Exception
    {
        public DoNotCorrectIdException()
        {
        }

        public DoNotCorrectIdException(string message) : base(message)
        {
        }

        public DoNotCorrectIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DoNotCorrectIdException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
