﻿using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ITaskService : ICrud<int, TaskDto>
    {
        Task<TaskDto> GetByIdWithDetails(int id, bool withComments);
       
    }
}
