﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ICrud<TKey, TModel> where TModel : class
    {
        Task<IEnumerable<TModel>> GetAllWithDetailsAsync(bool withdetail);
        Task<TModel> GetByIdAsync(TKey id);
        Task<int> AddAsync(TModel model);
        Task<TModel> UpdateAsync(int id, TModel model);
        Task DeleteByIdAsync(TKey modelId);
        //just for training purpose to see how status can be changed
        TaskTrackingContext Context { get; }
    }
}
 