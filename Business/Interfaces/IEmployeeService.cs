﻿using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IEmployeeService : ICrud<int, EmployeeDto>
    {
        Task<EmployeeDto> GetByIdWithDetailsAsync(int id, bool withTasks);
        Task<Employee> CheckEmpByEmailPassword(string email, string password);
    }
}
