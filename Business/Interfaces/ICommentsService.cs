﻿using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ICommentsService : ICrud<int, CommentDto>
    {
        Task<int> CreateCommentInsideTaskAsync(int id, CommentDto model);
        Task<IEnumerable<CommentDto>> GetCommentsByTaskIdAsync(int id);
        Task<CommentDto> UpdateAsync(int taskId, int commentId, CommentDto model);
        Task DeleteCommentByTaskIdAsync(int taskId, int commentId);
    }
}
