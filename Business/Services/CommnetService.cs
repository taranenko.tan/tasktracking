﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class CommnetService : ICommentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskTrackingContext Context => _unitOfWork.Context;

        public CommnetService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<IEnumerable<CommentDto>> GetAllWithDetailsAsync(bool withdetail)
        {
            IQueryable<Comment> comments = await _unitOfWork.CommentRepository.GetAllAsync();
            return comments.Select(c => _mapper.Map<Comment, CommentDto>(c));
        }
        public async Task<CommentDto> GetByIdAsync(int id)
        {
            Comment comment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
            return _mapper.Map<Comment, CommentDto>(comment);
        }
        private async Task<Mission> GetMissionByIdAsync(int id)
        {
            Mission mission = await _unitOfWork.MissionRepository.GetByIdWithDetailsAsync(id);

            if (mission == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Task ID => {id} (((");
            }
            return mission;
        }
        public async Task<IEnumerable<CommentDto>> GetCommentsByTaskIdAsync(int id)
        {
            Mission mission = await GetMissionByIdAsync(id);
            return mission.Comments.Select(c => _mapper.Map<Comment, CommentDto>(c));
        }
        public async Task<int> AddAsync(CommentDto model)
        {
            Comment comment = _mapper.Map<CommentDto, Comment>(model);
            await _unitOfWork.CommentRepository.CreateAsync(comment);
            await _unitOfWork.SaveAsync();
            return comment.Id;
        }
        public async Task<int> CreateCommentInsideTaskAsync(int id, CommentDto model)
        {
            Comment comment = _mapper.Map<CommentDto, Comment>(model);
            comment.MissionId = id;

            await _unitOfWork.CommentRepository.CreateAsync(comment);
            await _unitOfWork.SaveAsync();

            return comment.Id;
        }
        public async Task<CommentDto> UpdateAsync(int id, CommentDto model)
        {
            Mission mission = await this.GetMissionByIdAsync(id);

            Comment oldComment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
            if (oldComment == null) throw new DoNotCorrectIdException($"The Data Base, doesn`t have Comment with this ID => { id } (((");

            Comment newComment = _mapper.Map<CommentDto, Comment>(model);
            _mapper.Map(newComment, oldComment);

            await _unitOfWork.SaveAsync();
            return _mapper.Map<Comment, CommentDto>(oldComment);
        }
        public async Task<CommentDto> UpdateAsync(int taskId, int commentId, CommentDto newComment)
        {
            newComment.Id = commentId;
            var com = _mapper.Map<CommentDto, Comment>(newComment);
            com.MissionId = taskId;

            try
            {
                _unitOfWork.CommentRepository.Update(com);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Comment ID => {commentId} (((");

            }

            return newComment;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Comment comment = await _unitOfWork.CommentRepository.GetByIdAsync(modelId);
            if (comment == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Comment ID => {modelId} (((");
            }

            await _unitOfWork.CommentRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
        public async Task DeleteCommentByTaskIdAsync(int taskId, int commentId)
        {
            Mission task = await GetMissionByIdAsync(taskId);

            Comment comment = await _unitOfWork.CommentRepository.GetByIdAsync(commentId);
            if (comment == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Comment ID => {commentId} (((");
            }

            await _unitOfWork.CommentRepository.DeleteByIdAsync(commentId);
            await _unitOfWork.SaveAsync();
        }
    }
}
