﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class TaskBoardService : ITaskBoardService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public TaskTrackingContext Context => _unitOfWork.Context;
        private void GetInfoFromException(DbUpdateException e, TaskBoardDto model,
            out string innerId, out string table, int id = 0)
        {
            innerId = id.ToString();
            table = "TaskBoard";

            if (e.InnerException != null)
            {
                if (e.InnerException.Message.Contains("EmployeeId"))
                {
                    innerId = model.EmployeeInRoleId.ToString();
                    table = "EmployeeInRole";
                }
                else if (e.InnerException.Message.Contains("IssuedById"))
                {
                    innerId = model.IssuedByInRoleId.ToString();
                    table = "EmployeeInRole";
                }
                else if (e.InnerException.Message.Contains("MissionId"))
                {
                    innerId = model.Task.ToString();
                    table = "Mission";
                }
            }
        }

        public TaskBoardService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<TaskBoardDto>> GetAllWithDetailsAsync(bool withdetail)
        {
            var all = await _unitOfWork.TaskBorderRepository.GetAllWithDetails();
            return all
                    .AsEnumerable()
                    .Select(tb => _mapper.Map<TaskBoard, TaskBoardDto>(tb));
        }
        public async Task<TaskBoardDto> GetByIdAsync(int id)
        {
            var taskBoard = await _unitOfWork.TaskBorderRepository.GetByIdAsync(id);

            if (taskBoard == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have TaskBoard with this ID => { id } (((");
            var taskBoardDTO = _mapper.Map<TaskBoard, TaskBoardDto>(taskBoard);

            return taskBoardDTO;
        }
        public async Task<int> AddAsync(TaskBoardDto model)
        {
            TaskBoard newTaskBoard = _mapper.Map<TaskBoardDto, TaskBoard>(model);

            try
            {
                await _unitOfWork.TaskBorderRepository.CreateAsync(newTaskBoard);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException e)
            {
                string innerId, table;
                GetInfoFromException(e, model, out innerId, out table);

                throw new DoNotCorrectIdException($"The Data Base, doesn`t have {table} with this ID => { innerId } (((");
            }

            return newTaskBoard.Id;
        }
        public async Task<TaskBoardDto> UpdateAsync(int id, TaskBoardDto model)
        {
            model.Id = id;
            TaskBoard newTaskBoard = _mapper.Map<TaskBoardDto, TaskBoard>(model);

            try
            {
                _unitOfWork.TaskBorderRepository.Update(newTaskBoard);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException e)
            {
                string innerId, table;
                GetInfoFromException(e, model, out innerId, out table, id);

                throw new DoNotCorrectIdException($"The Data Base, doesn`t have {table} with this ID => { innerId } (((");
            }

            return model;
        }
        public async Task DeleteByIdAsync(int id)
        {
            try
            {
                await _unitOfWork.TaskBorderRepository.DeleteByIdAsync(id);
                await _unitOfWork.SaveAsync();
            }
            catch (ArgumentNullException)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have TaskBoard with this ID => { id } (((");
            }
        }
    }
}
