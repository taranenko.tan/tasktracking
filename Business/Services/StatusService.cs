﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class StatusService : IStatusService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public TaskTrackingContext Context => _unitOfWork.Context;

        public StatusService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<StatusDto>> GetAllWithDetailsAsync(bool withdetail)
        {
            IQueryable<Status> statuses = await _unitOfWork.StatusRepository.GetAllAsync();
            IQueryable<StatusDto> statusDTOs = statuses.Select(s => _mapper.Map<Status, StatusDto>(s));
            return statusDTOs.AsEnumerable();
        }
        public async Task<StatusDto> GetByIdAsync(int id)
        {
            Status status = await _unitOfWork.StatusRepository.GetByIdAsync(id);

            if (status == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Status Id => { id } (((");
            }

            StatusDto statusDTO = _mapper.Map<Status, StatusDto>(status);
            return statusDTO;
        }
        public async Task<int> AddAsync(StatusDto model)
        {
            Status status = _mapper.Map<StatusDto, Status>(model);
            await _unitOfWork.StatusRepository.CreateAsync(status);
            await _unitOfWork.SaveAsync();
            return status.Id;
        }
        public async Task<StatusDto> UpdateAsync(int id, StatusDto model)
        {
            Status status = await _unitOfWork.StatusRepository.GetByIdAsync(id);
            if (status == null) 
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Status Id => { model.Id } (((");

            status.StatusName = model.StatusName;
            await _unitOfWork.SaveAsync();
            return _mapper.Map<Status, StatusDto>(status);

        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Status status = await _unitOfWork.StatusRepository.GetByIdAsync(modelId);
            if (status == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Status Id => { modelId } (((");

            await _unitOfWork.StatusRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }


    }
}
