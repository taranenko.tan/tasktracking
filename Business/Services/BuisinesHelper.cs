﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public static class BuisinesHelper
    {
        public static string GetAllMessages(Exception e, string message)
        {
            message += $"Type=> {e.GetType()}:  {e.Message} \n";

            if (e.InnerException == null) return message;

            return GetAllMessages(e.InnerException, message);
        }
    }
}
