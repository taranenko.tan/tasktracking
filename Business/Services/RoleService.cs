﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{

    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public TaskTrackingContext Context => _unitOfWork.Context;

        public RoleService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<RoleDto>> GetAllWithDetailsAsync(bool withDetail)
        {
            IQueryable<Role> roles = await _unitOfWork.RoleRepository.GetAllAsync();
            IQueryable<RoleDto> rolesDTO = roles.Select(r => _mapper.Map<Role, RoleDto>(r));
            return rolesDTO.AsEnumerable();
        }
        public async Task<RoleDto> GetByIdAsync(int id)
        {
            Role role = await _unitOfWork.RoleRepository.GetByIdAsync(id);
            RoleDto roleDTO = _mapper.Map<Role, RoleDto>(role);

            if (roleDTO == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Role with this ID => { id } (((");
            }

            return roleDTO;
        }
        public async Task<int> AddAsync(RoleDto model)
        {
            Role role = _mapper.Map<RoleDto, Role>(model);
            await _unitOfWork.RoleRepository.CreateAsync(role);
            await _unitOfWork.SaveAsync();
            return role.Id;
        }
        public async Task<RoleDto> UpdateAsync(int id, RoleDto model)
        {
            Role role = await _unitOfWork.RoleRepository.GetByIdAsync(id);
            if (role == null) 
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Role with this Id => { id } (((");

            role.RoleName = model.RoleName;
            await _unitOfWork.SaveAsync();

            return _mapper.Map<Role, RoleDto>(role);
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Role role = await _unitOfWork.RoleRepository.GetByIdAsync(modelId);

            if(role == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Role with this ID => { modelId } (((");

            await _unitOfWork.RoleRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }


    }
}
