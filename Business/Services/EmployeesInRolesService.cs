﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class EmployeesInRolesService : IEmployeesInRolesService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private void GetInfoFromException(DbUpdateException e, EmployeesInRolesDto model,
           out string innerId, out string table, int id = 0)
        {
            innerId = id.ToString();
            table = "EmployeesInRoles";

            if (e.InnerException != null)
            {
                if (e.InnerException.Message.Contains("Employees"))
                {
                    innerId = model.UserId.ToString();
                    table = "Employees";
                }
                else if (e.InnerException.Message.Contains("Role"))
                {
                    innerId = model.RoleId.ToString();
                    table = "Role";
                }
            }
        }
        public TaskTrackingContext Context => _unitOfWork.Context;

        public EmployeesInRolesService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<EmployeesInRolesDto>> GetAllWithDetailsAsync(bool withdetail)
        {
            IQueryable<EmployeeInRole> empInRoles = await _unitOfWork.EmpolyeeInRoleRepository.GetAllWithDetailsAsync();
            return empInRoles.Select(e => _mapper.Map<EmployeeInRole, EmployeesInRolesDto>(e));
        }
        public async Task<EmployeesInRolesDto> GetByIdAsync(int id)
        {
            var empInRole = await _unitOfWork.EmpolyeeInRoleRepository.GetByIdAsync(id);
            if (empInRole == null)
                throw new DoNotCorrectIdException(
                    $"The Data Base, doesn`t have Employy witb this Id => {id } (((");

            return _mapper.Map<EmployeeInRole, EmployeesInRolesDto>(empInRole);
        }
        public async Task<int> AddAsync(EmployeesInRolesDto model)
        {
            EmployeeInRole empInRole = _mapper.Map<EmployeesInRolesDto, EmployeeInRole>(model);

            try
            {
                await _unitOfWork.EmpolyeeInRoleRepository.CreateAsync(empInRole);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException e)
            {
                string innerId, table;
                GetInfoFromException(e, model, out innerId, out table);

                throw new DoNotCorrectIdException($"The Data Base, doesn`t have {table} with this ID => { innerId } (((");
            }

            return empInRole.Id;
        }
        public async Task<EmployeesInRolesDto> UpdateAsync(int id, EmployeesInRolesDto model)
        {
            model.Id = id;
            EmployeeInRole empInRole = _mapper.Map<EmployeesInRolesDto, EmployeeInRole>(model);
            
            try
            {
                _unitOfWork.EmpolyeeInRoleRepository.Update(empInRole);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException e)
            {
                string innerId, table;
                GetInfoFromException(e, model, out innerId, out table);

                throw new DoNotCorrectIdException($"The Data Base, doesn`t have {table} with this ID => { innerId } (((");
            }
            
            return model;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            EmployeeInRole employeeInRole = await _unitOfWork.EmpolyeeInRoleRepository.GetByIdAsync(modelId);
            if (employeeInRole == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have EmployeeInRole with this ID => { modelId } (((");

            await _unitOfWork.EmpolyeeInRoleRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

    }
}
