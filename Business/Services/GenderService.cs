﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    //  _unitOfWork.Context.ChangeTracker.DetectChanges() => for watching state in debug;
    public class GenderService : IGenderService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public TaskTrackingContext Context => _unitOfWork.Context;

        public GenderService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<GenderDto>> GetAllWithDetailsAsync(bool withEmployees)
        {
            IQueryable<Gender> genders = null;

            if (withEmployees)
            {
                genders = await _unitOfWork.GenderRepository.GetAllWithDetailsAsync();
            }
            else
            {
                genders = await _unitOfWork.GenderRepository.GetAllAsync();
            }

            IQueryable<GenderDto> gendersDTO = genders
                .AsNoTracking()
                .Select(g => _mapper.Map<Gender, GenderDto>(g));

            return gendersDTO.AsEnumerable();
        }
        public async Task<GenderDto> GetByIdAsync(int id)
        {
            Gender gender = await _unitOfWork.GenderRepository.GetByIdAsync(id);
            if (gender == null) 
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Gender with this ID => { id } (((");

            return _mapper.Map<Gender, GenderDto>(gender);
        }
        public async Task<GenderDto> GetByIdWithDetailsAsync(int id, bool withEmployees)
        {
            Gender gender = null;

            if (withEmployees)
            {
                gender = await _unitOfWork.GenderRepository.GetByIdWithDetailsAsync(id);
            }
            else
            {
                gender = await _unitOfWork.GenderRepository.GetByIdAsync(id);
            }

            if (gender == null) throw new DoNotCorrectIdException($"The Data Base, doesn`t have this ID => { id } (((");

            return _mapper.Map<Gender, GenderDto>(gender);
        }
        public async Task<int> AddAsync(GenderDto model)
        {
            Gender entity = _mapper.Map<GenderDto, Gender>(model);

            //if (model.EmployeesId?.Count > 0)
            //{
            //    _unitOfWork.GenderRepository.AttachEmployees(entity.Employees);
            //}

            await _unitOfWork.GenderRepository.CreateAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity.Id;
        }
        public async Task<GenderDto> UpdateAsync(int id, GenderDto model)
        {
            model.Id = id;
            Gender gender = _mapper.Map<GenderDto, Gender>(model);

            try
            {
                _unitOfWork.GenderRepository.Update(gender);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateConcurrencyException )
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Gender with this ID => { id } (((");
            }

            return model;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            try
            {
                await _unitOfWork.GenderRepository.DeleteByIdAsync(modelId);             
                await _unitOfWork.SaveAsync();
            }
            catch (ArgumentNullException)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Gender with this ID => { modelId } (((");
            }
        }
    }
}
