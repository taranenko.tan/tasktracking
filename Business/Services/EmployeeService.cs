﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{

    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordHasher<Employee> _hasher;
        public TaskTrackingContext Context => _unitOfWork.Context;

        public EmployeeService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _hasher = new PasswordHasher<Employee>();
        }
        public async Task<IEnumerable<EmployeeDto>> GetAllWithDetailsAsync(bool withTasks)
        {
            IQueryable<Employee> allEmpss = await _unitOfWork.EmployeeRepository.GetAllWithDetailsAsync();
            //convert from IQueryable to List, because later I wanna change some of the property in employees in Foreach
            List<EmployeeDto> employees = allEmpss.Select(u => _mapper.Map<Employee, EmployeeDto>(u)).ToList();

            if (withTasks)
            {
                var allTaskBoards = await _unitOfWork.TaskBorderRepository.GetAllWithDetails();
                var taskBoards = allTaskBoards.ToList();

                foreach (var emp in employees)
                {
                    var task = taskBoards
                        .Where(t => emp.EmployeeInRole.Contains(t.EmployeeInRoleId))
                        .Select(t => t.Mission);
                    emp.Tasks = task.Select(t => _mapper.Map<Mission, TaskDto>(t));
                }
            }

            return employees.AsEnumerable();
        }
        public async Task<EmployeeDto> GetByIdAsync(int id)
        {
            var emp = await _unitOfWork.EmployeeRepository.GetByIdAsync(id);
            return _mapper.Map<Employee, EmployeeDto>(emp);
        }
        public async Task<EmployeeDto> GetByIdWithDetailsAsync(int id, bool withTasks)
        {
            var allEmp = await _unitOfWork.EmployeeRepository.GetAllWithDetailsAsync();
            var emp = allEmp.FirstOrDefault(u => u.Id == id);
            if (emp == null) throw new DoNotCorrectIdException($"The Data Base, doesn`t have this ID => { id } (((");
            EmployeeDto empDto = _mapper.Map<Employee, EmployeeDto>(emp);

            if (!withTasks)
            {
                return empDto;
            }

            var allTaskBoards = await _unitOfWork.TaskBorderRepository.GetAllWithDetails();
            var task = allTaskBoards
                        .Where(t => empDto.EmployeeInRole.Contains(t.EmployeeInRoleId))
                        .Select(t => t.Mission);
            empDto.Tasks = task.Select(t => _mapper.Map<Mission, TaskDto>(t));

            return empDto;
        }
        public async Task<Employee> CheckEmpByEmailPassword(string email, string password)
        {
            var allEmps = await _unitOfWork.EmployeeRepository.GetAllWithDetailsAsync();
            Employee employee = allEmps.FirstOrDefault(e => e.Email == email);

            if (employee == null)
                throw new DoNotCorrectIdException($"Your Email or Password was incorrect => (((");

            PasswordVerificationResult res = _hasher.VerifyHashedPassword(employee, employee.HashPassword, password);

            if (PasswordVerificationResult.Failed == res)
                throw new DoNotCorrectIdException($"Your Email or Password was incorrect => (((");

            return employee;
        }
        public async Task<int> AddAsync(EmployeeDto model)
        {
            var gender = await _unitOfWork.GenderRepository.GetEntityAsync(g => g.Title == model.Gender);
            if (gender == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Gender => { model.Gender } (((");

            Employee employee = _mapper.Map<EmployeeDto, Employee>(model);
            employee.Gender = gender;
            _unitOfWork.EmployeeRepository.Update(employee);
            await _unitOfWork.SaveAsync();

            return employee.Id;
        }
        public async Task<EmployeeDto> UpdateAsync(int id, EmployeeDto newModel)
        {
            Employee oldEmp = await _unitOfWork.EmployeeRepository.GetByIdAsync(id);
            if (oldEmp == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have Emoloyee with this Id => { id } (((");

            _mapper.Map(newModel, oldEmp);

            _unitOfWork.EmployeeRepository.Update(oldEmp);
            await _unitOfWork.SaveAsync();

            newModel.Age = (int)DateTime.Now.Subtract(DateTime.Parse(newModel.Birthday)).TotalDays / 365;
            newModel.Id = id;

            return newModel;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Employee employee = await _unitOfWork.EmployeeRepository.GetByIdAsync(modelId);
            if (employee == null)
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Employee => { modelId } (((");

            await _unitOfWork.EmployeeRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
    }
}
