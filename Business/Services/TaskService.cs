﻿using AutoMapper;
using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{

    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private void GetInfoFromException(DbUpdateException e, TaskDto model,
           out string innerId, out string table, int id = 0)
        {
            innerId = id.ToString();
            table = "Mission";

            if (e.InnerException != null)
            {
                innerId = model.Status.ToString();
                table = "Status";              
            }
        }
        public TaskTrackingContext Context => _unitOfWork.Context;

        public TaskService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<TaskDto>> GetAllWithDetailsAsync(bool withComments)
        {
            IQueryable<Mission> missions = null;

            if (withComments)
            {
                missions = await _unitOfWork.MissionRepository.GetAllWithDetailsAsync();
            }
            else
            {
                missions = await _unitOfWork.MissionRepository.GetAllAsync();
            }

            return missions.Select(t => _mapper.Map<Mission, TaskDto>(t));
        }
        public async Task<TaskDto> GetByIdWithDetails(int id, bool withComments)
        {
            Mission mission = null;

            if (withComments)
            {
                mission = await _unitOfWork.MissionRepository.GetByIdWithDetailsAsync(id);
            }
            else
            {
                mission = await _unitOfWork.MissionRepository.GetByIdAsync(id);
            }

            if (mission == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Task ID => {id} (((");
            }

            return _mapper.Map<Mission, TaskDto>(mission);
        }
        public async Task<TaskDto> GetByIdAsync(int id)
        {
            Mission mission = await _unitOfWork.MissionRepository.GetByIdAsync(id);
            if (mission == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Task ID => {id} (((");
            }

            return _mapper.Map<Mission, TaskDto>(mission);
        }
        public async Task<int> AddAsync(TaskDto model)
        {
            Mission mission = _mapper.Map<TaskDto, Mission>(model);

            try
            {
                await _unitOfWork.MissionRepository.CreateAsync(mission);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException)
            {
                throw new DoNotCorrectIdException(
                    $"The Data Base, doesn`t have Status with this ID => {model.Status} (((");
            }

            return mission.Id;
        }
        public async Task<TaskDto> UpdateAsync(int id, TaskDto model)
        {
            model.Id = id;
            Mission newTask = _mapper.Map<TaskDto, Mission>(model);

            try
            {
                _unitOfWork.MissionRepository.Update(newTask);
                await _unitOfWork.SaveAsync();
            }
            catch (DbUpdateException e)
            {
                string innerId, table;
                GetInfoFromException(e, model, out innerId, out table, id);

                throw new DoNotCorrectIdException($"The Data Base, doesn`t have {table} with this ID => { innerId } (((");
            }

            return model;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Mission task = await _unitOfWork.MissionRepository.GetByIdAsync(modelId);
            if (task == null)
            {
                throw new DoNotCorrectIdException($"The Data Base, doesn`t have this Task ID => {modelId} (((");
            }

            await _unitOfWork.MissionRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }


    }
}
