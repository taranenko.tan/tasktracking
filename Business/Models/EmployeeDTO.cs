﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Birthday { get; set; }

        public int Age { get; set; }
        public IEnumerable<int> EmployeeInRole;
        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}
