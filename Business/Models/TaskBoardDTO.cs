﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class TaskBoardDto
    {
        public int Id { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [Required]
        public int IssuedByInRoleId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [Required]
        public int EmployeeInRoleId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [Required]
        public int Task { get; set; }

        public string ManagerName { get; set; }
        public string EmployeeName { get; set; }
        public string TaskTitle { get; set; }
        //public string Status { get; set; }
        //public string StartProcess { get; set; }

    }
}
