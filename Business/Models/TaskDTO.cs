﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class TaskDto
    {

        public int Id { get; set; }
        public string BeginMission { get; set; }
        public string FinishMission { get; set; }
        public string Discription { get; set; }
        public string Completion { get; set; }
        [Required]
        public int Status { get; set; }
        public IEnumerable<string> Comments { get; set; }
    }
}
