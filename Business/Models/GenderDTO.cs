﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class GenderDto
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public List<int> EmployeesId { get; set; }
    }
}
