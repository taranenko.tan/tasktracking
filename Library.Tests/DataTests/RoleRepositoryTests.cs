﻿using Data.Entities;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Tests.DataTests
{
    [TestFixture]
    public class RoleRepositoryTests
    {
        private DbContextOptions<TaskTrackingContext> _options; 
        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }
        [Test]
        public async Task RoleRepository_GetAllWithDetails_ReturnAllValues()
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var roleRepository = new RoleRepository(context);

                //act
                var allRoles = await roleRepository.GetAllWithDetailsAsync();
                var roles = allRoles.ToList();

                //assert
                Assert.AreEqual(3, roles.Count);
                Assert.IsNotNull(roles[0].UserInRoles);
                Assert.AreEqual("Admin", roles[0].RoleName);
            }
        }

        [Test]
        public async Task RoleRepository_GetByIdWithDetails_ReturnValueById()
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var roleRepository = new RoleRepository(context);

                //act
                var reader = await roleRepository.GetByIdAsync(1);

                //assert
                Assert.IsNotNull(reader);
                Assert.AreEqual("Admin", reader.RoleName);
            }
        }
    
    }
}
