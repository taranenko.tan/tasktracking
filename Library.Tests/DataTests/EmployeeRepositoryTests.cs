﻿using Data.Entities;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Tests.DataTests
{
    [TestFixture]
    public class EmployeeRepositoryTests
    {
        private DbContextOptions<TaskTrackingContext> _options;
        [SetUp]
        public void SetUp()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public async Task EmployeeRepository_GetByIdWithDetails(int id)
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var employeeRepository = new EmployeesRepository(context);

                //act
                var employee = await employeeRepository.GetByIdWithDetailsAsync(id);
                var expected = ExpectedEmployees.FirstOrDefault(e => e.Id == id);

                //assert
                Assert.That(employee,
                    Is.EqualTo(expected).Using(new EmployeeEqualityComparer()));
            }
        }

        [Test]
        public async Task EmployeeRepository_GetAllWithDetails()
        {

            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var employeeRepository = new EmployeesRepository(context);

                //act
                var employees = await employeeRepository.GetAllWithDetailsAsync();
                var expected = ExpectedEmployees;

                //assert
                Assert.That(employees,
                    Is.EqualTo(expected).Using(new EmployeeEqualityComparer()));
            }
        }

        [Test]
        public async Task EmployeeRepository_Create()
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var employeeRepository = new EmployeesRepository(context);

                //act
                await employeeRepository.CreateAsync(new Employee { FirstName = "NewFirstName" });
                await context.SaveChangesAsync();

                //assert
                Assert.That(context.Employees.Count(), Is.EqualTo(4));
            }
        }

        [Test]
        public async Task EmployeeRepository_Delete()
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var employeeRepository = new EmployeesRepository(context);

                //act
                await employeeRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();
                var emps = await employeeRepository.GetAllAsync();
                var id = emps.FirstOrDefault().Id = 2;

                //assert
                Assert.That(context.Employees.Count(), Is.EqualTo(2));
                Assert.AreEqual(2, id);
            }
        }

        [Test]
        public async Task EmployeeRepository_Update()
        {
            using (var context = new TaskTrackingContext(_options))
            {
                //arrange
                var employeeRepository = new EmployeesRepository(context);
                var emp = await employeeRepository.GetByIdAsync(1);

                //act
                emp.LastName = "LastName";
                employeeRepository.Update(emp);
                await context.SaveChangesAsync();

                emp = await employeeRepository.GetByIdAsync(1);
                //assert
                Assert.That(emp.LastName == "LastName");
            }
        }

        private static IEnumerable<Employee> ExpectedEmployees =>
            new[]
            {
                new Employee{ Id=1, FirstName = "Liam", LastName = "Smith", Email = "liam@gmail.com", Birthday = new DateTime(1987, 10, 10),
                    Gender = new Gender{ Title="Male" }},
                new Employee {Id=2, FirstName = "Olivia", LastName = "Johnson", Email = "olivia@gmail.com", Birthday = new DateTime(1995, 12, 12),
                    Gender = new Gender{ Title="Female"}},
                new Employee {Id=3, FirstName = "Noah", LastName = "Williams", Email = "noah@gmail.com", Birthday = new DateTime(1994, 4, 25),
                    Gender = new Gender{ Title="Male" }}
            };

    }


}
