﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Tests
{
    internal class RoleEqualityComparer : IEqualityComparer<Role>
    {
        public bool Equals([AllowNull] Role x, [AllowNull] Role y)
        {
            if (x == null)
                return y == null;
            if (y == null)
                return x == null;

            return x.Id == y.Id && x.RoleName == y.RoleName;
        }

        public int GetHashCode([DisallowNull] Role obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class EmployeeEqualityComparer : IEqualityComparer<Employee>
    {
        public bool Equals([AllowNull] Employee x, [AllowNull] Employee y)
        {
            if (x == null)
                return y == null;
            if (y == null)
                return x == null;

            return x.Id == y.Id && x.FirstName == y.FirstName && x.LastName == y.LastName && x.Email == y.Email &&
                x.Birthday == y.Birthday && x.Gender.Title == y.Gender.Title;
        }

        public int GetHashCode([DisallowNull] Employee obj)
        {
            return obj.GetHashCode();
        }
    }
}
