import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { LocalStorageService } from "./localStorage.service";

@Injectable({
  providedIn: 'root'
})
export class GenericCrudService {
  public url: string;
  constructor(private http: HttpClient, private localStorage: LocalStorageService) {
  }

  getItems<returnType>(): Observable<returnType[]> {

    return this.http.get<returnType[]>(this.url, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.localStorage.get('token')}`)
    })
      .pipe(
        tap(data => console.log('All', JSON.stringify(data))),
        catchError(this.handlError)
      );
  }

  createItem<returnType>(item: returnType): Observable<returnType> {
    return this.http.post<returnType>(this.url, item, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.localStorage.get('token')}`)
    })
      .pipe(
        tap(data => console.log('All', JSON.stringify(data))),
        catchError(this.handlError)
      );
  }

  updateItem<returnType>(id: number, item: returnType): Observable<returnType> {
    return this.http.put<returnType>(`${this.url}/${id}`, item, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.localStorage.get('token')}`)
    })
      .pipe(
        tap(data => console.log('All', JSON.stringify(data))),
        catchError(this.handlError)
      );
  }

  deleteItem(id: number) {
    return this.http.delete(`${this.url}/${id}`, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.localStorage.get('token')}`)
    })
      .pipe(
        tap(data => console.log('All', JSON.stringify(data))),
        catchError(this.handlError)
      );
  }

  private handlError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      //A client-side or network error occured
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      //The unsucsseful return from the server
      errorMessage = `Server returned code: ${err.status}, error message is :${err.message}`;
    }
    return throwError(errorMessage);
  }
}
