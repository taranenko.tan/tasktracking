import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree }
  from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { LoginRequest, LoginResults } from "../models/loginResults.model";
import { LocalStorageService } from "./localStorage.service";
import jwt_decode from 'jwt-decode';
import { CurrentUser } from "../models/currentUser.model";

@Injectable()
export class AuthActivator implements CanActivate {
  constructor(private httpClient: HttpClient, private router: Router,
    private localStorage: LocalStorageService, private currentUser: CurrentUser) { }

  public currentPage = '';
  get expiration(): Date {
    return new Date(this.localStorage.get('date'));
  }
  get loginRequired(): boolean {
    return this.localStorage.get('token') === null ||
      this.localStorage.get('token') === undefined || this.expiration < new Date();
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  login(creds: LoginRequest) {
    return this.httpClient.post<LoginResults>('https://localhost:44310/api/account', creds)
      .pipe(map(data => {
        this.localStorage.set('token', data.token)
        this.localStorage.set('date', data.expiration.toString())
        this.parseToken();
      }), catchError(this.handleError));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree
    | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    this.currentPage = `${route.url}`;

    if (this.loginRequired) {
      this.router.navigate(["login"]);
      return false;
    } else {
      return true;
    }
  }

  parseToken() {
    let tokenInfo = this.getDecodedAccessToken(this.localStorage.get('token'));
    console.log(tokenInfo);
    this.currentUser.name = tokenInfo.sub;
    this.currentUser.role = tokenInfo.prn;
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }


}
