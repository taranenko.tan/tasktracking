import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { CurrentUser } from "../models/currentUser.model";
import { LoginRequest } from "../models/loginResults.model";
import { AuthActivator } from "../services/authActivator.service";


@Component({
  templateUrl: "loginPage.component.html"
})
export class LoginPage {

  constructor(public auth: AuthActivator, private router: Router) { }

  public creds: LoginRequest = {
    username: "",
    password:""
  }

  public errorMessage = "";

  onLogin() {

    this.auth.login(this.creds)
      .subscribe(() => {
        console.log('User was logged in sacssesfully');
        if (this.auth.canActivate) {
          this.router.navigate([`${this.auth.currentPage}`]);
        } else {
          this.router.navigate([""]);
        }
      }, error => {
          this.errorMessage = error;
      })
  }
}
