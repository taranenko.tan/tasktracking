import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { RoleAdminComponent } from './admin/roleAdmin.component';
import { GenderAdminComponent } from './admin/genderAdmin.component';
import { TaskAdminComponent } from './admin/taskAdmin.component';
import { EmployeeAdminComponent } from './admin/employeeAdmin.component';
import { EmployeesInRolesComponent } from './admin/employeesInRolesAdmin.component';
import { StatusAdminComponent } from './admin/statusAdmin.component';
import { TaskBoardAdminComponent } from './admin/taskboardAdmin.component';
import { LoginPage } from './pages/loginPage.component';
import { AuthActivator } from './services/authActivator.service';
import { CurrentUser } from './models/currentUser.model';
import { LocalStorageService } from './services/localStorage.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    RoleAdminComponent,
    GenderAdminComponent,
    TaskAdminComponent,
    EmployeeAdminComponent,
    EmployeesInRolesComponent,
    StatusAdminComponent,
    TaskBoardAdminComponent,
    LoginPage
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'genders', component: GenderAdminComponent, canActivate: [AuthActivator] },
      { path: 'roles', component: RoleAdminComponent, canActivate: [AuthActivator]},
      { path: 'tasks', component: TaskAdminComponent, canActivate: [AuthActivator] },
      { path: 'employees', component: EmployeeAdminComponent, canActivate: [AuthActivator] },
      { path: 'employeesInRoles', component: EmployeesInRolesComponent, canActivate: [AuthActivator] },
      { path: 'statuses', component: StatusAdminComponent, canActivate: [AuthActivator] },
      { path: 'taskboard', component: TaskBoardAdminComponent, canActivate: [AuthActivator] },
      { path: 'login', component: LoginPage },
      { path: "**", redirectTo:"/" }
    ])
  ],
  providers: [AuthActivator, RoleAdminComponent, CurrentUser, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
