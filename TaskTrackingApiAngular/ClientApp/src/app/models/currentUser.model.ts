import { Injectable, OnInit } from "@angular/core";
import { LocalStorageService } from "../services/localStorage.service";
import jwt_decode from 'jwt-decode';

@Injectable()
export class CurrentUser implements OnInit {
  constructor(private localStorage: LocalStorageService) { }

  public name?: string;
  public role?: string;


  get checkIfAdmin(): boolean {
    if (this.token !== null) {
      return this.token.prn !== "Admin"
    }
    return true;
  }

  private get token() {
    return this.getDecodedAccessToken(this.localStorage.get('token'));
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  logOut() {
    this.name = "";
    this.role = "";
    this.localStorage.remove('token');
    this.localStorage.remove('date');

    console.log('Token was deleted');
  }

  ngOnInit(): void {
    this.name = this.token.sub === null ? null : this.token.sub;
    this.role = this.token.prn === null ? null : this.token.prn;
  }



}
