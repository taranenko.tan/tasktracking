export class Employee {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public gender?: string,
    public birthday?: Date,
    public age?: number,
    public roles?: string[]

  ) { }
}
