export class EmployeeInRole {
  constructor(
    public id?: number,
    public userId?: number,
    public roleId?: number,
    public userName?: string,
    public roleName?: number,
  ) { }
}
