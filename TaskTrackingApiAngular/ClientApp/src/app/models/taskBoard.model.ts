export class TaskBoard {
  constructor(
    public id?: number,
    public issuedByInRoleId?: number,
    public employeeInRoleId?: number,
    public task?: number,
    public managerName?: string,
    public employeeName?: string,
    public taskTitle?: string
  ) { }
}
