
export class Task {
  constructor(
    public id?: number,
    public beginMission?: Date,
    public finishMission?: Date,
    public discription?: string,
    public completion?: string,
    public status?: number
  ) { }
}
