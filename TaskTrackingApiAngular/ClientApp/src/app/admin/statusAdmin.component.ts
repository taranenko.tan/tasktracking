import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { Status } from "../models/status.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/statuses';


@Component({
  templateUrl: "statusAdmin.component.html"
})
export class StatusAdminComponent implements OnInit, OnDestroy {

  public statuses: Status[];
  public status: Status;
  public tableMode: boolean;
  sub: Subscription;

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {
    this.tableMode = true;
  }
  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: statuses => this.statuses = statuses,
        error: err => console.log(err)
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createStatus(id: number) {
    this.service.createItem(this.status)
      .subscribe({
        next: status => {
          this.status = status;
          this.statuses.push(this.status);
          this.clearStatus();
        },
        error: err => console.log(err)
      });
  }

  updatStatus(id: number) {
    this.service.updateItem(id, this.status)
      .subscribe({
        next: gender => {
          this.status = gender;
          var res = this.statuses.find(r => r.id == id);
          res.statusName = this.status.statusName;
          this.clearStatus();
        },
        error: err => console.log(err)
      });
  }

  deleteStatus(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: gender => {
          this.statuses.forEach((role, index) => {
            if (role.id == id) this.statuses.splice(index, 1);
          })
          this.clearStatus();
        },
        error: err => console.log(err)
      })
  }

  selectStatus(id: number) {
    this.status = this.statuses.find(g => g.id == id);
  }

  clearStatus() {
    this.status = new Status();
    this.tableMode = true;
  }
}
