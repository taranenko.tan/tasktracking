import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { TaskBoard } from "../models/taskBoard.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/taskBoard';


@Component({
  templateUrl: "taskBoardAdmin.component.html"
})
export class TaskBoardAdminComponent implements OnInit, OnDestroy {

  public tasksBoards: TaskBoard[];
  public taskBoard: TaskBoard;
  public tableMode: boolean;
  sub: Subscription;

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {
    this.tableMode = true;
  }
  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: taskBoards => this.tasksBoards = taskBoards,
        error: err => console.log(err)
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createTaskBoard(id: number) {
    this.service.createItem(this.taskBoard)
      .subscribe({
        next: gender => {
          this.taskBoard = gender;
          this.tasksBoards.push(this.taskBoard);
          this.clearTaskBoard();
        },
        error: err => console.log(err)
      });
  }

  updateTaskBoard(id: number) {
    this.service.updateItem(id, this.taskBoard)
      .subscribe({
        next: gender => {
          this.taskBoard = gender;
          this.copyTaskBoard(id);
          this.clearTaskBoard();
        },
        error: err => console.log(err)
      });
  }

  deleteTaskBoard(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: taskBoard => {
          this.tasksBoards.forEach((res, index) => {
            if (res.id == id) this.tasksBoards.splice(index, 1);
          })
          this.clearTaskBoard();
        },
        error: err => console.log(err)
      })
  }

  copyTaskBoard(id: number) {
    var res = this.tasksBoards.find(r => r.id == id);
    res.issuedByInRoleId = this.taskBoard.issuedByInRoleId;
    res.employeeInRoleId = this.taskBoard.employeeInRoleId;
    res.task = this.taskBoard.task;
  }


  selectTaskBoard(id: number) {
    this.taskBoard = this.tasksBoards.find(g => g.id == id);
  }

  clearTaskBoard() {
    this.taskBoard = new TaskBoard();
    this.tableMode = true;
  }
}
