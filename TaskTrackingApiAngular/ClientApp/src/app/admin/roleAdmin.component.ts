import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { Role } from "../models/role.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/roles';

@Component({
  templateUrl: "roleAdmin.component.html"
})
export class RoleAdminComponent implements OnInit, OnDestroy {

  public roles: Role[];
  public role: Role;
  public tableMode: boolean;
  sub: Subscription;
  public token = "";
  public expiration = new Date();

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {

  }

  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: roles => this.roles = roles,
        error: err => console.log(err)
      })
    this.tableMode = true;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  get loginRequired(): boolean {
    return this.token.length === 0 || this.expiration < new Date();
  }

  //login(creds: LoginRequest) {
  //  return this.httpClient.post<LoginResults>('https://localhost:44310/api/account', creds)
  //    .pipe(map(data => {
  //      this.token = data.token;
  //      this.expiration = data.expiration;
  //    }), catchError(this.handleError));
  //}

  createRole(id: number) {
    this.service.createItem(this.role)
      .subscribe({
        next: role => {
          this.role = role;
          this.roles.push(this.role);
          this.clearRole();
        },
        error: err => console.log(err)
      })
  }

  updateRole(id: number) {
    this.service.updateItem(id, this.role)
      .subscribe({
        next: role => {
          this.role = role;
          var res = this.roles.find(r => r.id == id);
          res.roleName = this.role.roleName;
          this.clearRole();
        },
        error: err => console.log(err)
      })
  }

  deleteRole(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: role => {
          this.roles.forEach((r, index) => {
            if (r.id == id) this.roles.splice(index, 1);
          })
          this.clearRole();
        },
        error: err => console.log(err)
      })
  }

  selectRole(id: number) {
    this.role = this.roles.find(r => r.id == id);
  }

  clearRole() {
    this.role = new Role();
    this.tableMode = true;
  }
}
