import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { EmployeeInRole } from "../models/employeeInRole.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/employeesInRoles';

@Component({
  templateUrl: "employeesInRolesAdmin.component.html"
})
export class EmployeesInRolesComponent implements OnInit, OnDestroy {

  public employeesInRoles: EmployeeInRole[];
  public employeeInRole: EmployeeInRole;
  public tableMode: boolean;
  sub: Subscription;
  public token = "";
  public expiration = new Date();

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {

  }

  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: empInRoles => this.employeesInRoles = empInRoles,
        error: err => console.log(err)
      })
    this.tableMode = true;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  createEmployeeInRole(id: number) {
    this.service.createItem(this.employeeInRole)
      .subscribe({
        next: role => {
          this.employeeInRole = role;
          this.employeesInRoles.push(this.employeeInRole);
          this.clearEmployeeInRole();
        },
        error: err => console.log(err)
      })
  }

  updateEmployeeInRole(id: number) {
    this.service.updateItem(id, this.employeeInRole)
      .subscribe({
        next: empInRole => {
          this.employeeInRole = empInRole;
          this.copyEmployeesInRoles(id);
          this.clearEmployeeInRole();
        },
        error: err => console.log(err)
      })
  }

  deleteEmployeeInRole(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: role => {
          this.employeesInRoles.forEach((r, index) => {
            if (r.id == id) this.employeesInRoles.splice(index, 1);
          })
          this.clearEmployeeInRole();
        },
        error: err => console.log(err)
      })
  }

  copyEmployeesInRoles(id: number) {
    var res = this.employeesInRoles.find(r => r.id == id);
    res.roleId = this.employeeInRole.roleId;
    res.userId = this.employeeInRole.userId;
  }


  selectEmployeeInRole(id: number) {
    this.employeeInRole = this.employeesInRoles.find(r => r.id == id);
  }

  clearEmployeeInRole() {
    this.employeeInRole = new EmployeeInRole();
    this.tableMode = true;
  }
}
