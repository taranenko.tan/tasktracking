import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { Task } from "../models/task.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/tasks';


@Component({
  templateUrl: "taskAdmin.component.html"
})
export class TaskAdminComponent implements OnInit, OnDestroy {

  public tasks: Task[];
  public task: Task = new Task;
  public tableMode: boolean;
  sub: Subscription;

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {
    this.tableMode = true;
  }
  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: genders => this.tasks = genders,
        error: err => console.log(err)
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createTask() {
    this.service.createItem<Task>(this.task)
      .subscribe(result => {
        this.task = result;
        this.tasks.push(this.task);
        this.clearTask();
      });
  }

  updateTask(id: number) {
    this.service.updateItem(id, this.task)
      .subscribe({
        next: task => {
          this.task = task;
          this.copyTask(id);
          this.clearTask();
        },
        error: err => console.log(err)
      });
  }

  deleteTask(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: task => {
          this.tasks.forEach((role, index) => {
            if (role.id == id) this.tasks.splice(index, 1);
          })
          this.clearTask();
        },
        error: err => console.log(err)
      })
  }

  copyTask(id: number) {
    var res = this.tasks.find(r => r.id == id);
    res.beginMission = this.task.beginMission;
    res.finishMission = this.task.finishMission;
    res.discription = this.task.discription;
    res.status = this.task.status;
    res.completion = this.task.completion;
  }


  selectTask(id: number) {
    this.task = this.tasks.find(g => g.id == id);
  }

  clearTask() {
    this.task = new Task();
    this.tableMode = true;
  }
}
