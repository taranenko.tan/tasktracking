import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { Gender } from "../models/gender.model";
import { GenericCrudService } from "../services/GenericCrudService.service";


@Component({
  templateUrl: "genderAdmin.component.html"
})
export class GenderAdminComponent implements OnInit, OnDestroy {

  public genders: Gender[];
  public gender: Gender;
  public tableMode: boolean;
  sub: Subscription;
  private rolesUrl = 'https://localhost:44310/api/genders';

  constructor(private genericService: GenericCrudService, public currentUser: CurrentUser) {
    this.tableMode = true;
  }
  ngOnInit(): void {

    this.genericService.url = this.rolesUrl;

    this.sub = this.genericService.getItems()
      .subscribe({
        next: genders => this.genders = genders,
        error: err => console.log(err)
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createGender(id: number) {
    this.genericService.createItem(this.gender)
      .subscribe({
        next: gender => {
          this.gender = gender;
          this.genders.push(this.gender);
          this.clearGender();
        },
        error: err => console.log(err)
      });
  }

  updateGender(id: number) {
    this.genericService.updateItem(id, this.gender)
      .subscribe({
        next: gender => {
          this.gender = gender;
          var res = this.genders.find(r => r.id == id);
          res.title = this.gender.title;
          this.clearGender();
        },
        error: err => console.log(err)
      });
  }

  deleteGender(id: number) {
    this.genericService.deleteItem(id)
      .subscribe({
        next: gender => {
          this.genders.forEach((role, index) => {
            if (role.id == id) this.genders.splice(index, 1);
          })
          this.clearGender();
        },
        error: err => console.log(err)
      })
  }

  selectGender(id: number) {
    this.gender = this.genders.find(g => g.id == id);
  }

  clearGender() {
    this.gender = new Gender();
    this.tableMode = true;
  }
}
