import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CurrentUser } from "../models/currentUser.model";
import { Employee } from "../models/employee.model";
import { GenericCrudService } from "../services/GenericCrudService.service";

const url = 'https://localhost:44310/api/employees';


@Component({
  templateUrl: "employeeAdmin.component.html"
})
export class EmployeeAdminComponent implements OnInit, OnDestroy {

  public employees: Employee[];
  public employee: Employee;
  public tableMode: boolean;
  sub: Subscription;

  constructor(private service: GenericCrudService, public currentUser: CurrentUser) {
    this.tableMode = true;
  }
  ngOnInit(): void {

    this.service.url = url;

    this.sub = this.service.getItems()
      .subscribe({
        next: employees => this.employees = employees,
        error: err => console.log(err)
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createEmployee(id: number) {
    this.service.createItem(this.employee)
      .subscribe({
        next: gender => {
          this.employee = gender;
          this.employees.push(this.employee);
          this.clearEmployee();
        },
        error: err => console.log(err)
      });
  }

  updateEmployee(id: number) {
    this.service.updateItem(id, this.employee)
      .subscribe({
        next: employee => {
          this.employee = employee;
          this.copyEmployee(id);
          this.clearEmployee();
        },
        error: err => console.log(err)
      });
  }

  deleteEmployee(id: number) {
    this.service.deleteItem(id)
      .subscribe({
        next: employee => {
          this.employees.forEach((emp, index) => {
            if (emp.id == id) this.employees.splice(index, 1);
          })
          this.clearEmployee();
        },
        error: err => console.log(err)
      })
  }

  copyEmployee(id: number) {
    var res = this.employees.find(r => r.id == id);
    res.birthday = this.employee.birthday;
    res.firstName = this.employee.firstName;
    res.lastName = this.employee.lastName;
    res.gender = this.employee.gender;
    res.birthday = this.employee.birthday;
    res.age = this.employee.age;
  }

  selectEmployee(id: number) {
    this.employee = this.employees.find(g => g.id == id);
  }

  clearEmployee() {
    this.employee = new Employee();
    this.tableMode = true;
  }
}
