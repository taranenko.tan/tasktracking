﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Data.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TaskBoardController : ControllerBase
    {
        private readonly ITaskBoardService _service;
        private readonly ILogger<TaskBoardController> _logger;
        private readonly LinkGenerator _linkGenerator;

        public TaskBoardController(ITaskBoardService service, ILogger<TaskBoardController> logger, LinkGenerator linkGenerator)
        {
            _service = service;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                return Ok(await _service.GetAllWithDetailsAsync(false));
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskBoardDto>> Get(int id)
        {
            try
            {
                return await _service.GetByIdAsync(id);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPost]
        public async Task<ActionResult<TaskBoardDto>> Post(TaskBoardDto model)
        {
            try
            {
                model.Id = await _service.AddAsync(model);
                string location = _linkGenerator.GetPathByAction("Get", "TaskBoard", new { id = model.Id });
                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TaskBoardDto>> Put(int id, TaskBoardDto model)
        {
            try
            {
                return await _service.UpdateAsync(id, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete (int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }
    }
}
