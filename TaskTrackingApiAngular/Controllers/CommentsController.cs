﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/tasks/{id}/comments")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentsService _service;
        private readonly ILogger<CommentsController> _logger;
        private readonly LinkGenerator _linkGenerator;

        public CommentsController(ICommentsService commentService, ILogger<CommentsController> logger, LinkGenerator linkGenerator)
        {
            _service = commentService;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                return Ok(await _service.GetCommentsByTaskIdAsync(id)); 
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpGet("{taskId:int}")]
        public async Task<ActionResult> Get(int id, int taskId)
        {
            try
            {
                return Ok(await _service.GetByIdAsync(id));
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Post(int id, CommentDto model)
        {
            try
            {
                model.Id = await _service.CreateCommentInsideTaskAsync(id, model);
                var location = _linkGenerator.GetPathByAction("Get", "Comments", new { id = model.Id });

                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{commentId:int}")]
        public async Task<ActionResult<CommentDto>> Put(int id, int commentID, CommentDto model)
        {
            try
            {
                return await _service.UpdateAsync(id, commentID, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{commentId:int}")]
        public async Task<IActionResult> Delete(int id, int commentId)
        {
            try
            {
                await _service.DeleteCommentByTaskIdAsync(id, commentId);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

    }
}
