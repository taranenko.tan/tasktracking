﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StatusesController : ControllerBase
    {
        private readonly ILogger<StatusesController> _logger;
        private readonly IStatusService _service;
        private readonly LinkGenerator _linkGenerator;

        public StatusesController(ILogger<StatusesController> logger, IStatusService service, LinkGenerator linkGenerator)
        {
            _logger = logger;
            _service = service;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatusDto>>> Get()
        {
            try
            {
                return Ok(await _service.GetAllWithDetailsAsync(false));
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<StatusDto>> Get(int id)
        {
            try
            {
                return await _service.GetByIdAsync(id);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPost]
        //We do not need to type fromBody cause we use [ApiController] attribute
        //and also for this reason we do not use {if (modelStateIsValid)}
        public async Task<ActionResult<StatusDto>> Post(StatusDto model)
        {
            try
            {
                model.Id = await _service.AddAsync(model);
                string location = _linkGenerator.GetPathByAction("Get", "Statuses", new { id = model.Id });
                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<StatusDto>> Put (int id, StatusDto model)
        {
            try
            {
                return await _service.UpdateAsync(id, model);
            }
            catch(DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpDelete("{Id:int}")]
        public async Task<IActionResult> Delete (int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }
    }
}
