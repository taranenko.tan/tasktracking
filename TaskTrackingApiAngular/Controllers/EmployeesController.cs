﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _service;
        private readonly ILogger<EmployeesController> _logger;
        private readonly LinkGenerator _linkGenerator;

        public EmployeesController(IEmployeeService service, ILogger<EmployeesController> logger, LinkGenerator linkGenerator)
        {
            _service = service;
            _logger = logger;
            this._linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeDto>>> Get(bool withTasks = false)
        {
            try
            {
                return Ok(await _service.GetAllWithDetailsAsync(withTasks));
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<EmployeeDto>> Get(int id, bool withTasks = false)
        {
            try
            {
                var gender = await _service.GetByIdWithDetailsAsync(id, withTasks);
                return gender;
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPost]
        //We do not need to type fromBody cause we use [ApiController] attribute
        //and also for this reason we do not use {if (modelStateIsValid)}
        public async Task<ActionResult<EmployeeDto>> Post(EmployeeDto model)
        {
            //We do not need to type fromBody cause we use [ApiController] attribute
            //and also for this reason we do not use {if (modelStateIsValid)}
            try
            {
                model.Id = await _service.AddAsync(model);

                //we use this Link Generator cause we do not depend on some specific route
                var location = _linkGenerator.GetPathByAction("Get", "Employees", new { id = model.Id });
                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<EmployeeDto>> Post(int id, EmployeeDto model)
        {
            try
            {
                return await _service.UpdateAsync(id, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete (int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }
    }
}
