﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _service;
        private readonly ILogger<EmployeesController> _logger;
        private readonly LinkGenerator _linkGenerator;

        public TasksController(ITaskService service, ILogger<EmployeesController> logger, LinkGenerator linkGenerator)
        {
            _service = service;
            _logger = logger;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDto>>> Get(bool withComments = false)
        {
            try
            {
                return Ok(await _service.GetAllWithDetailsAsync(withComments));
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDto>> Get(int id, bool withComments = false)
        {
            try
            {
                return await _service.GetByIdWithDetails(id, withComments);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPost]
        //We do not need to type fromBody cause we use [ApiController] attribute
        //and also for this reason we do not use {if (modelStateIsValid)}
        public async Task<ActionResult<TaskDto>> Post(TaskDto model)
        {
            try
            {
                model.Id = await _service.AddAsync(model);
                var location = _linkGenerator.GetPathByAction("Get", "Tasks", new { id = model.Id });
                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TaskDto>> Put(int id, TaskDto model)
        {
            try
            {
                return await _service.UpdateAsync(id, model);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete (int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }
    }
}
