﻿using Business.Exceptions;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTrackingApiAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmployeesInRolesController : ControllerBase
    {
        private readonly ILogger<EmployeesInRolesController> _logger;
        private readonly IEmployeesInRolesService _servise;
        private readonly LinkGenerator _linkGenerator;

        public EmployeesInRolesController(ILogger<EmployeesInRolesController> logger, IEmployeesInRolesService servide,
            LinkGenerator linkGenerator)
        {
            _logger = logger;
            _servise = servide;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeesInRolesDto>>> Get()
        {
            try
            {
                return Ok(await _servise.GetAllWithDetailsAsync(false));
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<EmployeesInRolesDto>> Get(int id)
        {
            try
            {
                return await _servise.GetByIdAsync(id);
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPost]
        //We do not need to type fromBody cause we use [ApiController] attribute
        //and also for this reason we do not use {if (modelStateIsValid)}
        public async Task<ActionResult<EmployeesInRolesDto>> Post(EmployeesInRolesDto model)
        {
            try
            {
                model.Id = await _servise.AddAsync(model);
                string location = _linkGenerator.GetPathByAction("Get", "EmployeesInRoles", new { id = model.Id });
                return Created(location, model);
            }
            catch (DoNotCorrectIdException e)
            {

                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<EmployeesInRolesDto>> Put(int id, EmployeesInRolesDto model)
        {
            try
            {
                return await _servise.UpdateAsync(id, model);
            }
            catch (DoNotCorrectIdException e)
            {

                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _servise.DeleteByIdAsync(id);
                return Ok();
            }
            catch (DoNotCorrectIdException e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                string exMessageAll = "";
                exMessageAll = BuisinesHelper.GetAllMessages(e, exMessageAll);

                _logger.LogError(exMessageAll);
                return StatusCode(500, exMessageAll);
            }
        }


    }
}
